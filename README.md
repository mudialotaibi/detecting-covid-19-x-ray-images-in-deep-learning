# Detecting COVID-19 X-ray images in deep learning

# Download dataset: https://drive.google.com/file/d/18aMf57_1u2AWInnMB67s3Xku0sPzm28u/view

# Dataset sources:
# We choose these eleven datasets because they are open source and fully available to the researchers: •Normal images:

# 1- ChestX-ray8 dataset [1], with a total of 5000 images.
# •Pneumonia images:

# 2- Chest X-Ray Images (Pneumonia) dataset [2], with a total of 4237 images, and 763 images from ChestX-ray8 dataset.[1].

# •COVID-19 images:

# 3- BIMCV-COVID19 dataset [3], with a total of 2473 images.

# 4- COVID-19 Image Data Collection [4], with a total of 208 images.

# 5- COVID-19 data from Figure 1 COVID-19 Chest X-ray Dataset [5], with a total of55 images.

# 6- COVID-19 data from the ActualMed COVID-19 Chest X-ray Dataset [6], with atotal of 238 images.

# 7- SIRM database [7], with a total of 68 images.

# 8- Twitter data [8], with a total of 37 images.

# 9- COVID-19 Repository [9], with a total of 243 images.

# 10- COVID-CXNet [10], with a total of 877 images.

# 11- MOMA-Dataset [11], with a total of 221 images.





